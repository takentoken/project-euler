// Largest palindrome product
fn is_palindrome(num: u32) -> bool {
    let mut reversed: u32 = 0;
    let mut number = num;
    while number > 0 {
        reversed = number % 10 + 10 * reversed;
        number /= 10;
    }
    reversed == num
}

fn main() {
    let mut largest_palindrome: u32 = 0;
    let mut a = 100;
    while a <= 999 {
        let mut b = 100;
        while b <= 999 {
            if is_palindrome(a * b) && a * b > largest_palindrome {
                largest_palindrome = a * b;
            }
            b += 1;
        }
         a += 1;
    }
    println!("The largest num is {}", largest_palindrome);
}