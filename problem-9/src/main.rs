// Special Pythagorean triplet
fn main() {
    // 设周长为p = 1000，a <= b < c
    // 则可得a < 1000 / 3, b = p(p - 2a) / 2(p - a), b是整数
    let p = 1000i32;
    for a in 1..p / 3 {
        let n = p.pow(2) - 2 * p * a;
        let d = 2 * p - 2 * a;
        if n % d == 0 {
            let b = n / d;
            let c = p - a - b;
            println!("The multiple is {}", a * b * c);
        }
    }
}