// Largest prime factor
fn main() {
    let mut number: u64 = 600851475143;
    let mut factor: u64 = 2;
    while number > 1 {
        if number % factor == 0 {
            number /= factor;
        } else {
            factor += 1;
        }
    }
    println!("The largest prime factor is {}", factor);
}