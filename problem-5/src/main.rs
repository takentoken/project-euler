// Smallest multiple
fn take_prime_number_less_than_a_number(num: u32) -> Vec<u32> {
    let mut res = Vec::new();
    for i in 2..=num {
        if is_prime(i) {
            res.push(i);
        }
    }
    res
}

fn is_prime(num: u32) -> bool {
    if num == 1 {return false;}
    for i in 2..num {
        if num % i == 0 {
            return false;
        }
    }
    return true;
}

fn main() {
    let num = 20;
    let prime_factor = take_prime_number_less_than_a_number(num);
    let mut ans = 1;
    for i in prime_factor.iter() {
        let mut n = 1;
        while i.pow(n) < num {
            n += 1;
        }
        ans *= i.pow(n-1);
    }
    println!("{:?}", ans);
}