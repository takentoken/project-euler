// Even Fibonacci numbers
fn main() {
    let mut prev = 1;
    let mut curr = 2;
    let mut sum = 0;
    let mut temp;
    while curr < 4_000_000 {
        if curr % 2 == 0 {
            sum += curr;
        }
        temp = prev;
        prev = curr;
        curr = temp + prev;
    }
    println!("The sum is {}", sum);
}
