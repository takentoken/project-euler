// Highly divisible triangular number
// 还可以优化
use std::collections::HashMap;

fn get_prime_factors(num: u64) -> HashMap<u64, u32> {
    let mut factors: HashMap<u64, u32> = HashMap::new();
    let a = 2;
    let mut m = num;
    while m > 1 {
        let mut i = a;
        while i <= m {
            if m % i == 0 {
                let counter = factors.entry(i).or_insert(0);
                *counter += 1;
                m = m / i;
            }
            else {i += 1};
        }
    }
    factors
}

fn number_of_divisors(num: u64) -> u32 {
    let factors = get_prime_factors(num);
    let mut res = 1u32;
    for i in factors.keys() {
        res = res * (factors[i] + 1);
    }
    res
}

fn main() {
    let mut n = 7;
    while number_of_divisors(n*(n+1)/2) <= 500u32 {
        n += 1;
    }
    println!("{:?}", n*(n+1)/2);
}