// Summation of primes
// 太慢了，有待改进
fn main() {
    let mut sum = 2u64;
    let mut curr = 3u64;
    let is_prime = |x: &u64| (2..=(*x as f64).sqrt() as u64).all(|n| x % n != 0);
    while curr < 2_000_000u64 {
        if is_prime(&curr) {sum += curr;}
        curr += 2;
    }
    println!("{}", sum);
}