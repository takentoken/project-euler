// 10001st prime
fn is_prime(num: u32) -> bool {
    if num == 1 {return false;}
    let num_as_float = num as f64;
    for i in 2..(num_as_float.sqrt().floor() as u32) {
        if num % i == 0 {
            return false;
        }
    }
    return true;
}

fn main() {
    // 根据π(x)，小于100000的素数有9592个
    let mut idx = 9592;
    let mut cur = 10u32.pow(5) - 1;
    while idx <= 10001 {
        cur += 2;
        if is_prime(cur) {
            idx += 1;
        }
    }
    println!("The 10001th prime number is {}", cur);
}