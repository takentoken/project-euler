fn sum_of_square_of_number(num: u32) -> u32 {
    (2 * num + 1) * (num + 1) * num / 6
}

fn square_of_sum_of_number(num: u32) -> u32 {
    (num * (num + 1)).pow(2) / 4
}

fn main() {
    let num = 100;
    println!("The answer is {}", square_of_sum_of_number(num) - sum_of_square_of_number(num));
}